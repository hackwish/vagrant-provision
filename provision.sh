#!/bin/bash

# Actualizando repositorios
sudo apt-get update
sudo apt-get upgrade

#Instalando utilidades
sudo apt-get -y install vim

# Instalando Apache 2
sudo apt-get -y install apache2

# Instalando PHP
sudo apt-get -y install php5 libapache2-mod-php5 mcrypt php5-mcrypt php5-cgi php5-cli php5-curl php5-common php5-gd php5-mysql

#Agregando módulos y reiniciando servicio
sudo php5enmod mcrypt

sudo service apache2 restart 


#Instaler MySQL
#sudo apt-get -y install mysql-server

#OPCIONAL - instalar LAMP completa con un solo comando
#sudo apt-get -y install server-lamp^

#Post-conf Apache
#CONFIGURACION USUARIO APACHE
useradd -gwww-data -d /home/www/html webmaster
chown -R webmaster.www-data /home/www/
chmod -R 755 /home/www
#passwd webmaster

#CAMBIAR PAGINA POR DEFECTO
#echo -e "<?php\nheader('Location: http://www.google.cl');\nexit();"> /home/www/html/index.php

#CAMBIAR PARAMETROS APACHE
ln -s /etc/apache2 /apache
cp /apache/apache2.conf /apache/apache2.conf.orig

#CAMBIAR RUTA POR DEFECTO APACHE
mv /var/www/html/index.html /var/www/
rmdir /var/www/html
ln -s /home/www/html /var/www/html
#mv /var/www/index.html /var/www/html/
exit

exit 0


